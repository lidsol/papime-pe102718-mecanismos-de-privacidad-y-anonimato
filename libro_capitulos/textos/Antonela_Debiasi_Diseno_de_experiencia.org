* Diseño de experiencia de usuario en el Proyecto Tor: Una mirada abierta
#+latex: \authdata{Antonela Debiasi}

Mucho antes de ser parte del equipo de experiencia de usuario en el
Proyecto Tor, pasé semanas leyendo los canales públicos de IRC de la
comunidad. Ahí noté que *la palabra /design/ se utilizaba para
referirse a una gran diversidad de aspectos*: el /software/, la red,
los dibujos, la topología, el código, y la lista sigue. Confuso, pero
igual positivo. La amplitud del concepto, es la prueba fáctica de que
el proceso de diseño es un método proyectual que atraviesa distintas
disciplinas. En tal caso, es natural /vivir el proceso de diseño como
un trabajo abierto/.

En el Proyecto Tor se considera que la manera en que trabajamos nos
define, por eso decidimos *trabajar de manera abierta y colaborativa*.

Creemos que la forma en la que trabajamos —la modalidad de los
procesos— tiene una relación directa con lo que producimos. Si
cambiamos la forma de trabajar, transformamos los resultados de la
producción. Tiqqunim, /l'organe de liaison au sein du Parti
Imaginaire/, cita a Yann Moulier-Boutang en su manifiesto ``La
Hipótesis Cibernética'':

#+begin_quote
``Su teleología ya no es la del proletariado o de la naturaleza, sino
la del Capital. Su perspectiva es profundamente en la actualidad la de
una economía social, de una “economía solidaria”, de una
“transformación del modo de producción”, no ya por colectivización o
estatización de los medios de producción, sino por colectivización de
las decisiones de producción. Como lo muestra por ejemplo un Yann
Moulier Boutang, finalmente de lo que se trata es de que sea
reconocido “el carácter social colectivo de la creación de
riqueza(...)''
#+end_quote

Abierto, remoto y libre. Este es el modo de trabajo que *aplicamos a
nuestra metodología de diseño*. Así es como logramos un proceso de
diseño abierto donde los desarrolladores y la comunidad trabajan
juntos para mejorar el /software/, y el equipo de diseño *funciona
como un conector de ideas*, como un puente.

Usando esta modalidad de trabajo abierta, hicimos iteraciones internas
basadas en conceptos surgidos de investigaciones anteriores, de ideas
de usuarios y de sugerencias en tickets, que en su conjunto nos
ayudaron a definir el flujo más seguro e intuitivo para nuestros
usuarios.

** ¿Cómo funciona el proceso de trabajo abierto?

Lo primero que debemos entender es que hay momentos de divergencia y
momentos de convergencia. Y a su vez, es importante mantener el ritmo
entre ellos. Aquí el proceso:

1. Partimos del *planteo de un problema*. Ese es nuestro inicio. No es
   exactamente el problema que tenemos que resolver, pero sí es una
   descripción que sirve como síntoma para identificar la
   problemática.
2. Exploramos el problema y cuáles son los distintos *interrogantes
   que dispara el análisis*. Estas preguntas sencillas, de no más de
   ocho palabras, nos definen el espectro del problema a resolver y
   sus alcances. Una vez que visualizamos estos aspectos, comenzamos a
   pensar en cómo resolverlos.
3. Finalmente, el abanico de posibilidades se vuelve a abrir. Repensar
   el problema y proyectar soluciones implica *analizarlo desde
   distintas perspectivas*. La única manera de que esto ocurra es
   teniendo un equipo diverso.
4. Testeamos con usuarios y volvemos al punto 1.

El *equipo de experiencia de usuario* del Proyecto Tor trabaja de
forma horizontal con casi todos los equipos que forman parte del
proyecto:

- Equipo de red :: Desarrolla el backend de Tor, básicamente, todo que
                   envía y recibe bytes de la red
- Equipo de /metrics/ :: Se encarga de medir la red
- Equipo de aplicaciones :: Desarrolla y mantiene Tor Browser y Tor
     Browser para Android
- Equipo de comunidad :: Encargado de establecer vínculos entre
     organizaciones que defienden derechos humanos, como Karisma en
     Colombia o Derechos Digitales en Chile.

Respecto a este último punto de la comunidad, podemos hacer una
aclaración: *la situación geográfica de un usuario no debería
modificar su condición de ser humano*. Los usuarios censurados
viviendo en países opresores tienen las mayores dificultades para
lograr comunicaciones seguras y anónimas.

Como parte de nuestra iniciativa del sur global, nos conectamos con
comunidades que se encuentren físicamente entre la línea del ecuador y
el círculo polar antártico, como India, Uganda, Colombia y
Kenia. Viajamos y *realizamos pruebas de usabilidad*, en su mayoría
con usuarios que se definen a sí mismos como ``usuarios del día a
día'' —haciendo alusión a su relación con la tecnología—; pero también
aplicamos las pruebas con usuarios que se reconocen como /technical
experts/.

** ¿Cuáles son los resultados?

En las evaluaciones realizadas, el 93% de personas a las que llegamos
dijeron que creían que necesitaban alguna clase de protección
online. Siguiendo el *método de los cinco usuarios*, realizamos
*rondas de pruebas de usabilidad* sobre algunas mejoras específicas
que trajeron luz sobre los diversos modelos mentales, y el nivel de
conocimiento técnico que tienen nuestros usuarios.

Conocimos a personas como Juana, que vive en Colombia. Ella es
agricultora y parte de una productora de café autogestionada que usa
Tor para tener comunicaciones seguras entre las compañeras del
grupo. También conocimos a Jon, un activista ambiental y periodista en
Hoima, Uganda, que está utilizando Tor para publicar en su blog de
forma anónima.

Esta inmersión sirve para validar —o no— nuestras respuestas en
distintos contextos.

Alison Macrina —líder del equipo de comunidad, activista y feminista—
y yo —como parte del equipo de usabilidad— visitamos Hoima en abril
de 2018. Fuimos a dictar un taller de seguridad digital y a realizar
pruebas de usuarios con un grupo de activistas ambientales. Hoima es
una ciudad petrolera que queda a 200 kilómetros de Kampala, la capital
de Uganda, donde viven unas 30.000 personas. África del este. A los
cinco minutos de iniciado el taller, se cortó la luz. ¡Imagínense este
contexto! Todos detrás de sus computadoras, con Windows 98, se corta
la electricidad y todos se quedan sin internet.

Varios modelos de amenazas —incluidos el secuestro de dispositivos
electrónicos como computadoras portátiles por parte de la policía
local, o el actual partido político de turno, obligando a periodistas
a desclasificar sus fuentes— fueron comunes en la mayoría de esas
comunidades.

Parte del trabajo con las pruebas de usabilidad, nos permitió llegar a
las personas que utilizan nuestro /software/ en condiciones extremas,
como por ejemplo una infraestructura deficiente, paquetes de datos muy
costosos o hardware muy antiguo.

La principal reflexión en este proceso, es que sería egoísta no
preguntarnos sobre estos contextos y los valores que definen la toma
de decisiones en la práctica. *Crear una tecnología que respete al
usuario es una decisión de diseño*.

Por lo tanto, conocer la realidad de nuestros usuarios nos ayudó a
entender su contexto, empatizar con ellos y pensar soluciones
adaptadas a sus necesidades.

Los integrantes del equipo de Tor creemos que, si somos capaces de
hacer que nuestro producto sea usable por usuarios sin conocimientos
técnicos, los usuarios avanzados también podrán hacerlo. No queremos
distribuir software sin educación. Queremos empoderar a nuestros
usuarios a través de la educación.

Hoy, tener un switcher expuesto en la interfaz de usuario, que permita
al usuario decidir si compartir o no /—opt-in/opt-out—/ sus datos a
terceros, es una decisión política.

#+begin_quote
*Queremos educar a los usuarios para que puedan tener el control de su
navegación en internet.*
#+end_quote

** ¿Cómo se manifiestan los resultados?

El proceso de trabajo abierto que fue aplicado en todos los equipos
del proyecto, sumado a los análisis resultantes de las pruebas de
usuarios, incentivaron *grandes mejoras en la interfaz de usuario y en
el desarrollo del /software/*.

*Tor Browser 8* lanzó en septiembre de 2018 las siguientes
características:

- Indicadores de seguridad para sitios .onion
- Nueva pantalla de circuito
- /Onboarding/ para nuevos usuarios 

*** Indicadores de seguridad para sitios .onion

Antes de la actualización a Tor Browser 8, cuando los usuarios
visitaban un sitio onion, recibían como /feedback/ de seguridad —con la
mejor de la suertes— un signo de información [i]. Y si visitaban un
sitio onion sobre http, les aparecía el famoso candado rojo.

En pos de aumentar la confianza de los /onion services/, *decidimos
identificar a los servicios onion con un icono de cebolla*. Ahora, cada
vez que un usuario visita un sitio onion, un icono refleja el estado
de seguridad del sitio, contemplando sus certificados y alertando
sobre sus posibles riesgos.

*** Nueva pantalla de circuito

La pantalla de circuito o /circuit display/, es un componente de la
interfaz de usuario de Tor Browser, y parte de su experiencia. Cuando
se escribe una url en la barra de direcciones, el navegador hace una
conexión a la red tor. Este diagrama muestra cómo se ha realizado la
conexión entre nodos dentro de la red tor.

Mirando el /display/, los usuarios pueden rastrear su conexión y
mejorar la comprensión sobre cómo Tor ha construido su circuito,
identificar su nodo guarda y pedir un nuevo circuito si lo necesitan.

*** /Onboarding/ para nuevos usuarios

Como parte de nuestra iniciativa de educación del usuario, pensamos el
primer acercamiento del usuario al navegador como una oportunidad de
usar metáforas locales y *palabras simples, para explicar conceptos
complejos*.

Todos los mitos tecnológicos surgen de una falta de
comprensión. Nuestro /onboarding/ apunta a ayudar a establecer
confianza para nuestros usuarios, y nos esforzamos por comenzar
enseñando cómo la privacidad y seguridad de Tor pueden *proteger a los
usuarios en diferentes niveles*: la capa de aplicación y la capa de
red.

** Conclusión

Todos los integrantes del equipo del Proyecto Tor, creemos firmemente
que *educar usuarios es empoderarlos*, y que la *infraestructura
define la experiencia*.

El acceso a tecnologías de la privacidad y el anonimato en internet,
es clave para crear entornos seguros de intercambio y reflexión. La
forma más transparente y escalable de facilitar el acceso, es creando
lazos entre comunidades vecinas y vulnerables, y *distribuyendo
/software/ usable para todo el mundo*.

** Bibliografía y recursos

- https://www.usenix.org/system/files/conference/soups2018/soups2018-habib-prying.pdf
- https://users.ece.cmu.edu/~mahmoods/publications/chi17-cross-cultural-study.pdf
- https://www.researchgate.net/publication/327994051_Turtles_Locks_and_Bathrooms_Understanding_Mental_Models_of_Privacy_Through_Illustration
- https://duckduckgo.com/download/Private_Browsing.pdf
- https://www.usenix.org/conference/usenixsecurity18/presentation/winter
- https://www.bamsoftware.com/papers/thesis/
- https://svn.torproject.org/svn/projects/design-paper/tor-design.pdf
- https://link.springer.com/content/pdf/10.1007%2F978-3-642-23315-9_20.pdf
- https://www.nngroup.com/articles/why-you-only-need-to-test-with-5-users/
- https://www.nngroup.com/articles/technology-myths/
- https://tiqqunim.blogspot.com/2013/01/la-hipotesis-cibernetica.html
- http://opendesignkit.org

** Imágenes
*** TB8 - Release
- https://blog.torproject.org/new-release-tor-browser-80

*** TB8 - Onion Security Indicator
- https://www.dropbox.com/s/wk6im4rowsplwvc/old-new.png?dl=0

*** TB8 - Circuit Display
- https://trac.torproject.org/projects/tor/attachment/ticket/23247/060618.png
- https://www.dropbox.com/s/uly3m58yoy712eh/old-new%202.png?dl=0
- https://www.dropbox.com/s/xd7pj2w0x2y1wwx/mobile.png?dl=0
- ﻿https://www.dropbox.com/s/f81a3blye57qk14/24309%20-%20tba%20-%20tablets.png?dl=0

*** TB8 - Onboarding
- https://trac.torproject.org/projects/tor/attachment/ticket/25695/25695.png
- https://trac.torproject.org/projects/tor/attachment/ticket/25695/25695-about-new-feature-walkthrough.png
- https://www.dropbox.com/s/4gmvgvg5d9hfrwm/25696%20-%20TBA%20-%20Welcome%20Page.png?dl=0
- https://www.dropbox.com/sh/rxptqbdv40itq71/AADX3JXEz2ta1buS2v3Abovra?dl=0
- https://www.dropbox.com/s/xt9x0dppiwqov9r/TTB8-onboarding-1.png?dl=0
