Investigación de usuarios y software libre

sajolida, Tails

Hola buenos días, me llamo sajolida y trabajo para Tails.

Voy a empezar
por presentar Tails brevemente. Luego
compartiré con vosotros algunos de los métodos que estamos
usando en Tails para asegurarnos que sea cada vez más fácil de
usar, lo que llamamos *usabilidad*. Tenemos aquí varia gente que
desarrolla software libre y pienso que es un tema relevante para ayudarnos en
nuestro labor. Compartiré algunas reflexiones sobre la investigación de usuario,
que son las prácticas que llevamos a cabo a la hora de entender qué necesitan
las usuarias; tanto para responder a sus necesidades, como para comprobar que estas
necesidades estén cumplidas y que la gente consiga usar nuestros programas.
Porque pienso que hay algunos conflictos y algunos roces con la cultura que
veo entorno al software libre.

Qué es Tails?
=============

Tails es un sistema operativo portable que protege tu privacidad y ayuda a
evitar la censura. Un sistema operativo,
como por ejemplo Windows, macOS o Linux, un entorno de computación completo, pero en vez de
instalarlo en el disco duro del ordenador,
Tails se instala en una memoria USB.
Para usar Tails, apagaremos la computadora,
enchufaremos el USB en la computadora y arrancaremos la computadora desde el USB
únicamente, en vez del disco duro, sin tener que usar el sistema operativo instalado en el ordenador.
Luego Tails es un sistema operativo bastante convencional con sus
ventanas, su entorno de escritorio, sus menús y muchas aplicaciones.

Algunas de
las propiedades interesantes de Tails son:

- Es software libre. Es una distribución Linux basada en Debian y que
depende de muchos otros proyectos de software libre como Tor, una red
para el anonimato, o
GNOME, el entorno gráfico por defecto en Debian y en Ubuntu.

- Tails es un sistema portable. Cuando usemos
Tails, sólo correrá en la memoria RAM del ordenador y cuando lo apaguemos,
no dejaremos ningún rastro de lo que hicimos en la computadora.
Esto nos permite, por ejemplo, llevar nuestro USB de vacaciones y usar
el ordenador del ciber café sin dejar nuestros datos
en el ordenador o sin depender del sistema operativo que estuviera instalado en
él.

- Tails también ayuda a evadir la censura en Internet
porque todo el tráfico que sale del ordenador está
pasando por la red Tor, tanto el navegador,
como los emails, las mensajerías, etc.

- Al ser un sistema
operativo completo, también actúa como una caja de herramientas para
la privacidad. Tails incluye muchas herramientas de privacidad
que vienen instaladas y configuradas con la seguridad
en mente: el navegador de Tor, el cliente de
correo Thunderbird, el gestor
de contraseñas KeePassXC, OnionShare para compartir archivos, etc.;
todas listas para
usar.

Al hablar de los usos, el usuario más famoso de Tails quizás sea
Edward Snowden. Dijo
Dijo que todos los periodistas que reportaron las revelaciones sobre la
vigilancia de masas en el 2013 dependían de Tails y trabajaban con él
únicamente a través de Tails. Ampliando un poco el abanico de posibilidades,
presentaré brevemente 3 ejemplos:

- Periodistas
en México usan Tails para investigar violaciones de derechos
humanos por parte de empresas privadas.
Cuando hacen investigación en Internet sobre estas empresas,
lo hacen desde Tails
para que las empresas no sepan quiénes les está investigando.
Luego lo usan para comunicarse de
manera segura con las comunidades afectadas por estas mismas empresas.

- El National
Democratic Institute en los Estados Unidos usó Tails hace unos años para crear
un sistema de recogida de información en torno a unas elecciones en
Bielorrusia. Enviaron observadores internacionales que recogían información o
incidencias de lo que pasaba durante estas elecciones de manera segura desde Tails.

- Una asociación de profesionales de la seguridad, también en Estados Unidos,
está trabajando con casas de acogida para sobrevivientes de
violencia doméstica para que usen Tails. Si, por ejemplo,
tu marido te maltrata, quizás también este vigilando lo que
haces en tu ordenador o que programas tienes instalados en tu teléfono.
Tails te puede dar un entorno más seguro y
permitirte evitar la vigilancia aunque estéis viviendo debajo del mismo techo.

Nuestro proceso para la usabilidad
==================================

En los últimos años, me dediqué en hacer cada
vez más investigación de usabilidad dentro del proyecto para asegurarnos que nuestros programas
sean cada vez más fácil de usar. La usabilidad siendo un problema bastante común en el
mundo de la privacidad y del software libre. Voy a compartir con ustedes
algunos de los métodos que usamos.
 
Paso cero: Qué diseñar?
-----------------------

Antes de empezar a diseñar algo, tenemos que reflexionar sobre qué diseñar,
pensar a qué necesidad responde y entender porque estamos
creando este programa en primer lugar. Este es un debate muy amplio en el cual no voy a
entrar en detalle.

Paso uno: Prototipos en papel
-----------------------------

Una vez tomamos la decisión de
desarrollar una nueva funcionalidad o un nuevo programa, qué metodología podemos
usar para lograr que este programa sea fácil de usar?

Esto es lo que
llamamos *usabilidad* y todas las prácticas que nos llevan estas conclusiones
es lo que llamamos *investigación de usuario*. El primer paso que
practicamos en repetidas ocasiones
fue de hacer un diseño que podamos testear con usuarias, antes
de escribir cualquier línea de código. Una herramienta que
parece muy sencilla pero que también es muy mágica es hacer prototipos en papel.

[[!img src="prototyping.jpg"]]

Esta es una foto de un test de usabilidad que hicimos hace unos
meses. Dibujamos las ventanas del sistema operativo y las
ventanas de los programas que van a aparecer.
Trabajamos con usuarias representativas: les damos un lápiz que
usaran de mouse, para hacer clic, o de teclado, para escribir algo.

Me gusta
hacer esta metodología junto con un desarrollador. Somos tres personas en la
sala: un diseñador, la persona que va luego escribir el código y
una usuaria. La usuaria pretende usar el programa pero en
base a los dibujos en papel. El diseñador cambia los dibujos en la mesa para abrir una ventana, una
aplicación, etc.

Para crear los dibujos usamos el programa *WireframeSketcher*.
No es software libre, es software
propietario, pero funciona con Linux, macOS y Windows. Funciona incluso con
Tails y tiene una versión de demo. Permite dibujar estas
ventanas de manera muy rápida.

Podemos dibujar el
prototipo de papel de la primera idea que tengamos y luego testearla.
Cuándo se acaba la prueba, podemos extraer una serie de problemas y
corregirlos de manera muy fácil, cambiando sólo los dibujos o haciendo
dibujos nuevos. Esto nos permite ir mejorando el diseño cada vez que testeamos con una persona nueva
y así mejorar el diseño de manera muy rápida a lo
largo del día. Hicimos varios sprints de 3 días, empezando de cero: sacar
un primer diseño en un par de horas, testear, mejorar, testear otra vez, mejorar otra vez, etc. Al
acabar los 3 días ya sabíamos que lo que íbamos a desarrollar
iba a ser razonablemente fácil de usar.

Hacer así prototipos de papel, o de baja fidelidad con otras técnicas, nos
permite un trabajo:

- **Más rápido** porque nos ahora escribir código
que luego no funciona con la gente y que tendríamos que tirar a la basura. También nos evita tener
debates eternos dentro del equipo sobre lo que va a funcionar, dónde estarán los
problemas, si tenemos que hacer A o B, etc. Podemos probar la primera idea que tengamos y
validar o invalidarla inmediatamente con usuarias.
También nos ahorra tiempo porque los mismos dibujos sirven de especificaciones
a la persona que va a escribir el código.
El desarrollador
está con nosotros en la sala y ve como funciona el programa en
directo.

- **Mejor** porque nos evita diseñar
algo que luego no sea posible de implementar o
demasiado complicado y nos permite ajustar a lo que es factible.
Teniendo un desarrollador en la sala, también
le permite proponer soluciones o tener ideas que los diseñadores no
hubieran tenido o que hubiéramos creído demasiado complicadas.
Así nos ajustamos el diseño al
máximo de las posibilidades técnicas.

- **Más entregado** porque,
al confrontar los desarrolladores a las usuarias,
al ver las personas pelear con el diseño, y al haber colaborado en la resolución de estos
problemas, los desarrolladores tienen un incentivo muy fuerte a la hora de trabajar en las
soluciones. Es un gran trabajo de empatía.

Un libro que me gusta mucho sobre el tema y que habla también
de metodologías de testeo de usabilidad general es el _Paper Prototyping_ de [Carolyn Snyder].

[Carolyn Snyder]: http://gen.lib.rus.ec/book/index.php?md5=C42914FD52E1201FE23F46202AD709A5

Paso dos: Código
----------------

Una vez tenemos un prototipo en papel
y sabemos que funciona para las usuarias, podemos empezar a picar el código.

Paso tres: Pruebas de usabilidad
--------------------------------

Después de tener una primera versión del software, nos
parece importante volver a hacer otras pruebas
de usabilidad.

Reclutamos una usuaria y le vamos a proponer hacer
unas tareas concretas pero con el software ya funcionando en la computadora. Le damos
una computadora, unas tareas por cumplir y
le observamos mientras piensa en
voz alta: sobre lo que entiende del programa, qué
problemas tiene, lo que está haciendo, etc.
El objetivo es entender lo que está pasando en su cabeza e intentar nos influenciarlo.

La recomendación habitual es de hacer estas pruebas con 5
personas. Si lo hacemos con menos de 5 personas, hay un riesgo más alto de no detectar
algunos problemas importantes. Pero si lo hacemos con más de 5 personas, volveremos a
detectar los mismos problemas una y otra vez y no estaríamos haciendo un buen uso de
nuestro tiempo. La recomendación de testear con 5 personas optimiza la cantidad de problemas
identificados por el tiempo invertido.

Me gusta grabar las
pruebas porque las suelo hacer solo. No se entiende todo al momento
y, al volver a mirar la grabación, veo más matices y más detalles.
Los vídeos también son útiles a la hora de
comunicar con los desarrolladores.
Cambia mucho si explicas un problema o si lo
ven con sus propios ojos.

Para grabar la pantalla (*screencast*) usamos *Kazam*. Para grabar con una camera externa usamos *VLC*.

[Steve Krug] tiene un buen capitulo sobre estas pruebas de usabilidad en su libro
_Don't make me think_.

[Steve Krug]: http://gen.lib.rus.ec/book/index.php?md5=C2C1C494745E3F17A86A2F00BD8A9944

Paso cuatro: Priorizar los problemas
------------------------------------

De las pruebas de usabilidad vamos a sacar un listado de problemas
y priorizarlos.

El nuestro método, sacamos un listado y apuntamos, para cada problema:

- Qué fue el problema: una frase, empezado con un verbo, que describe lo que ocurrió.
- Con qué participante o participantes ocurrió.
- Cuánto es de importante resolver este problema (*Beneficio*):
  si es un problema muy grave, si es un problema que ha ocurrido
  muchas veces, etc. Suelo usar una escala del 1 al 3.
- Una posible solución.
- Cuánto costaría implementar esta solución (*Costo*). Se puede usar una escala
  del 1 al 3 o una sucesión de Fibonacci, como en el desarrollo ágil de software.

[[!img src="rainbow.png"]]

Calculando el *Beneficio/Costo* podemos priorizar los problemas y arreglar los más importantes primero,
optimizando así el uso de nuestros recursos para el mayor beneficio de las usuarias.

Paso cinco: Arreglar los problemas
----------------------------------

Con este listado priorizado de
problemas podemos volver a picar código para arreglarlos.

Si tenéis más tiempo
podéis volver a hacer más pruebas de usabilidad, más arreglos e
iterar y seguir mejorando la usabilidad cada vez.

Investigación de usuarios y software libre
==========================================

En esta tercera
parte compartiré unas reflexiones sobre lo que llamamos *investigación de
usuario*, este conjunto de prácticas que sirven para entender las usuarias,
sus problemas y sus necesidades. Hablaré de algunos roces o conflictos que
siento en la manera y la cultura que tenemos a la hora de desarrollar
software libre.

La cultura de reporte de bug
----------------------------

En el mundo del software libre es habitual
depender de lo que llamamos los *reportes de bug*: informes que nos envían usuarias por
listas de correo o sistemas de bug tracker, como GitHub. Pero muchas
veces los desarrolladores nos quedamos a la espera de que la gente nos
vaya explicando sus problemas en vez de buscar,
de manera más proactiva, cuáles son estos
problemas.

Creo que esto conlleva varios
problemas:

1. La usabilidad también depende de problemas muy
pequeños, de problemas de vocabulario, de problemas de contraste, de problemas de posicionamiento de los elementos en la interfaz, de
cosas que la gente no necesariamente piensa en reportarnos y pueden ser muy
importantes a pesar de ser pequeños.

2. Nos falta información
sobre la gravedad de estos problemas. Saber de un problema no nos dice
siempre cuánto es de grave: cuánta gente está afectada y como de grave está afectada.

3. Ver los problemas en primera persona, ver a la gente pelear con el
programa nos generará más empatía y más ganas de arreglarlos porque los
vamos a vivir de manera más personal.

La minoría vocal
----------------

También tenemos que preguntarnos *quién* reporta bugs.

Tails tenemos unas 23,000 usuarias diarias. Si hacemos la aproximación muy grosera de que
una persona quizás use Tails una vez a la semana,
quizás tengamos unas 150,000 usuarias regulares. En 2018, en nuestro bug tracker
sólo tuvimos 32 usuarias activas. Sería como 1 usuaria en
5,000, o 0.02 %. La realidad es que casi nadie que usa Tails nos reporta bugs.

[Matthew Paul Thomas], en un artículo
sobre usabilidad y software libre en 2008, dijo que:

« Si no se hacen pruebas de usabilidad frecuentemente, los proyectos dependen
de retornos subjetivos por parte de pocas personas muy motivadas. Pero lo que
dicen estas personas no es necesariamente representativo del conjunto de
usuarias, ni tanto solo de su propio uso. »

[Matthew Paul Thomas]: https://web.archive.org/web/20080805012124/http://mpt.net.nz:80/archive/2008/08/01/free-software-usability

La gente que es muy vocal en las listas y en los bugs
trackers es una minoría ínfima y tenemos que buscar maneras de
conectar con la inmensa mayoría silenciosa, las 4,999 usuarias que no van a reportarnos
bugs.

La mayoría silenciosa
---------------------

Una herramienta que nos está sirviendo en Tails para conectar con más
gente es *WhisperBack*, una herramienta para
informar de un error directamente desde Tails.

En el escritorio y hay un lanzador que abre *WhisperBack*, dónde
puedes explicar qué estabas haciendo y qué problema tuviste. Además
nos enviará toda una serie de información técnica sobre el hardware, la
configuración y los sucesos del sistema para que tengamos la información técnica necesaria para entender
el problema.

En 2018, recibimos reportes de aproximadamente 500
usuarias, 15 veces más que la gente que nos está reportando bugs.

En 2003, [David Nichols y Michael Twidale], en otro análisis de la usabilidad del
software libre dijeron que:

« Reportes de incidencias integrados son muy buenos
para resolver problemas de usabilidad en proyectos de código abierto. Es decir,
hacer que los usuarios reporten sus problemas al momento de tenerlos durante el
uso de la aplicación. »

[David Nichols y Michael Twidale]: http://firstmonday.org/ojs/index.php/fm/article/view/1018/939

Los patches (no siempre) son bienvenidos
----------------------------------------

Otro aspecto de la cultura de software libre es decir
que somos una comunidad, que nuestro código es abierto, que todo el mundo puede contribuir,
que todo el mundo puede enviarnos parches y que, si el código es bueno,
lo pondremos todo en el programa. Pero si vamos aceptando así parches, contribuciones o peticiones sin
criterio de usabilidad, construiremos un programa que será más difícil de usar.

[Havoc Pennington], hablando en este caso del proyecto GNOME, el
entorno de escritorio, escribió en 2002:

« Se le puede dar al usuario literalmente una infinidad de
opciones. (*Cada uno puede pedir lo que quiere que el programa haga.*) Pero cada una
de estas opciones tiene un coste de usabilidad (*porque va a ser una opción más,
una decisión más que el usuario tendrá que tomar, un elemento más
en la interfaz, una posibilidad más de equivocarse o dudar, etc.*) Entonces un programa con opciones infinitas es infinitamente
malo. El trabajo del diseñador es seleccionar
cuáles opciones son realmente
útiles. »

[Havoc Pennington]: https://ometer.com/preferences.html

A veces es complicado decir que "no" y más fácil decir que "sí"
pero, una vez una
opción o una funcionalidad implementada, es más difícil todavía quitarla,
porque la gente la está usando, se acostumbró a ella.

Como podéis ver, todos los artículos que cito tienen más de 10 años. No es
nada nuevo pero la situación no ha cambiado mucho.

Análisis de causa raíz
----------------------

En vez de prestar tanta atención a las peticiones de esta minoría vocal,
es importante pensar en
cuáles son las raíces de los problemas y no siempre quedarnos con lo que la gente nos
cuenta.

Por ejemplo, si soy médico y un paciente llega con un dolor de
espalda y me pide ibuprofeno. Quizás le tenga que decir que el
ibuprofeno no es la solución a su dolor de espalda y ayudarle a buscar la
raíz del problema.

Como metodología para este tipo de análisis, Sakichi Toyoda propone preguntarnos
5 veces ¿porqué?, para llegar a la raíz del problema.

Quizás el dolor de espalda viene de una mala postura en su puesto de trabajo o
de una posición demasiado estática. Quizás esta mala postura viene de de
un puesto de trabajo mal diseñado. Quizás el puesto de trabajo está mal
diseñado porque nunca se hizo un análisis del impacto de su ergonomía sobre la
salud y la productividad de los empleados, etc. Quizás una baja médica costosa
para la empresa y una carta al responsable de las oficinas serían una mejor
solución que el ibuprofeno.

Observar en vez de escuchar
---------------------------

En vez de escuchar al usuario, suele ser
más importante *observarlo*. [Jakob Nielsen], un gurú de la usabilidad,
dijo de manera un poco provocativa que la primera regla de la usabilidad es
no escuchar a las usuarias.
Hay una diferencia importante entre lo que
la gente *dice* y lo que la gente *hace* e incluso lo que la gente *necesita*.

[Jakob Nielsen]: https://www.nngroup.com/articles/first-rule-of-usability-dont-listen-to-users/

A la hora de seleccionar nuestros métodos de
investigación de usuarios, se hace la diferencia entre:

- **Las técnicas conductuales** que tratan de observar y de entender lo
que la gente hace realmente. Ya hemos hablado de las pruebas de usabilidad
y los prototipos de baja fidelidad.
En cierta medida, los programas como *WhisperBack*, que permiten a las usuarias reportar
un problema en el momento de tenerlo, reducen la diferencia entre lo que la
persona piensa y lo que la persona dice; también nos aportan datos técnicos sobre lo que realmente pasó.

- **Las técnicas actitudinales** que tratan
de lo que la gente dice también tienen su sentido. Por ejemplo, podemos hacer
entrevistas de las usuarias para entender mejor quiénes son, en qué contexto
trabajan, qué desean, etc.
Por ejemplo, si hacemos encuestas para recoger
datos cuantitativos, tendremos que tener en cuenta que es una técnica actitudinal y que habrá una
diferencia entre lo que la gente dice y lo que la gente realmente hace.
Las listas de correo y los reportes de bug también nos
dan información sobre lo que la gente dice más que sobre lo que realmente hace.

Tenemos
que adecuar nuestras técnicas a cada
una de nuestras preguntas de investigación.

Para un inventario de las muchísimas técnicas que existen para la investigación de
usuarios, clasificadas entre conductuales y
actitudinales, cuantitativas y cualitativas, véase este artículo de [Christian Rohrer].

[Christian Rohrer]: https://www.nngroup.com/articles/which-ux-research-methods/

Acabo con un pequeño resumen de lo dicho hasta ahora:

| No hacer                                  | Hacer                              |
|-------------------------------------------+------------------------------------|
| Escribir el código primero, luego testear | Testear, luego escribir el código  |
| Escuchar lo que las usuarias dicen        | Observar lo que las usuarias hacen |
| Prestar atención a la minoría vocal       | Aprender de la mayoría silenciosa  |
| Cumplir con peticiones                    | Entender la raíz del problema      |
| Decir que "si" por defecto                | Aprender a decir que "no"          |

Participante del público:

La herramienta que mencionó para los reportes de error de manera integrada ya
en Tails, es una herramienta libre que ustedes solo añadieron a Tails o es una
herramienta que ustedes desarrollaron?

sajolida:

El equipo de Tails desarrolló *WhisperBack* y está configurada específicamente para Tails.
Pero es una herramienta que podría
ser más genérica y, con un poco de trabajo en Debian, se podría
instalar en otras distribuciones.
Los reportes se envían de manera cifrada a un
servidor de mail nuestro pero eso también se puede configurar.

Participante del público:

La pregunta es más bien de la elección de GNOME.
No he leído las especificaciones
pero imagino que en equipos viejos o relativamente no tan nuevos ha
de costar un poco de trabajo correrlo. Tampoco estoy muy familiarizado con la
versión actual de GNOME pero la última que probé era bastante pesada.
No sé si se ha tomado en cuenta en los requerimientos o posibilidad de
países como México o Latinoamérica donde las computadoras no son tan nuevas.

sajolida:

Nuestra elección de GNOME es también una elección para dar prioridad a un
entorno que está más integrado y más usable, aunque quizá esté un poco más
pesado. Ahora los requerimientos para usar Tails son 2 GB de RAM y un ordenador
con un procesador 64-bit.

Según los datos de telemetría de Mozilla, 87% de las usuarias de Firefox en el
mundo tiene un procesador 64-bit y 3% tienen sólo 1 GB de RAM o menos. Entonces
creo que los requerimientos de Tails van bastante de par con la realidad de las
computadores en el mundo.

Participante del público:

Nada más felicitarlos por el proyecto. Sí se ha reflejado la usabilidad con
el paso del tiempo. Ahorita apenas baje la imagen, hice el procedimiento y se me
hace claro e intuitivo, incluso más avanzado que otras distribuciones. Por ejemplo,
te pregunta cuándo quieres verificar que instale desde el navegador un
plugin. Me
parece un acierto para aumentar el nivel de usuarios porque muchas
distribuciones de Linux no hacen eso. Te mandan a otro software, te mandan para
que lo verifiques en la línea de comandos. Creo que es un acierto de
Tails que se preocupen por eso, que vaya generando más usuarios, porque ahí es
clara la forma también el objetivo que tienen de verificar las imágenes.

sajolida:

Justamente cuando empezamos a preocuparnos por la usabilidad, lo primero
que ocurrió es que nos invitaron a hacer pruebas de usabilidad en una start-up
en París, NUMA. Preparamos algunas tareas un poco avanzadas del tipo: "enviar un
mensaje cifrado a otra persona en la sala". Lo que pasó es que la gente no
podía ni instalar Tails, ni arrancarlo, ni conectarse a Internet! Fue un
fracaso total para la gente y una epifanía para nosotros. Darnos cuenta que
nadie consiguió hacer una cosa que pensábamos que no era tan difícil.

Desde ahí
trabajamos mucho en el proceso de instalación. Empezamos haciendo un mapeo de lo
que teníamos en la web. Modificamos este mapeo para
quitar algunas opciones, poner algunas opciones más adelante, quitar
algunas etapas y asegurarnos que todo el proceso fuera más lineal.
También testeamos varias
veces todo el proceso y hicimos 3 o 4 iteraciones de las nuevas herramientas e instrucciones de instalación.

Efectivamente, lo que dices de la verificación para nosotros
es muy importante porque es un tema de seguridad muy clave para el uso de Tails.
Es un problema muy complicado para muchos proyectos. Muchos te dan una
imagen que nadie sabe usar o te dan un hash que tampoco nadie sabe usar.

Otra buena noticia es que en unos meses será más fácil todavía instalar Tails,
en Linux y Windows pero sobre todo en macOS, dónde ahora es casi imposible.
