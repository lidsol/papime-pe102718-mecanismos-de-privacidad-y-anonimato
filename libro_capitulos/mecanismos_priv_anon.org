#+title: Mecanismos de privacidad y anonimato en redes
#+subtitle: Una visión transdisciplinaria
#+author: Gunnar Wolf
#+date:
#+language: es
#+options: toc:0 num:1
#+latex_class: book
#+LaTeX_HEADER: \usepackage[x11names]{xcolor}
#+LaTeX_HEADER: \usepackage{lipsum}
#+LaTeX_HEADER: \hypersetup{linktoc = all, colorlinks = true, urlcolor = DodgerBlue4, citecolor = PaleGreen1, linkcolor = black}
#+latex_header: \usepackage[hyphens]{url}
#+latex_header: \usepackage[spanish]{babel}
#+latex_header: \usepackage{fancyhdr}
#+latex_header: \usepackage{listings}
#+latex_header: \usepackage[[ignoremp,papersize={14cm,21cm},textwidth=10.2cm,textheight=19.5cm,margin=1.5cm, includeall,nomarginpar,centering]{geometry}
#+latex_header: \newcommand{\authorHdr}{}
#+latex_header: \newcommand{\authdata}[1]{ { \vskip 0.5cm  \raggedleft \textit{#1}  \vskip 1cm \,} \addtocontents{toc}{#1} \renewcommand{\authorHdr}{#1} }
#+latex_header: \newcommand{\dice}[1]{ \vskip 0.5em \noindent \textit{#1}: \\ \noindent}
#+latex_header: \pagestyle{fancy}

# #+latex_header: \fancyhead[L,C,R]{}
#+latex_header: \fancyfoot[L,C,R]{}
#+latex_header: \fancyhead[RE]{\textsc{\authorHdr}}
# #+latex_header: \fancyhead[LO]{\thechapter. \textsc{\chaptermark}}
#+latex_header: \fancyfoot[LE,RO]{\thepage}
#+latex_header: \renewcommand{\headrule}{}
#+latex_header: \renewcommand{\chaptermark}[1]{\markboth{\textsc{\thechapter.\ #1}}{}}

#+latex_header: \def\captionsspanish{ \def\contentsname{Índice} \def\partname{Parte} }

#+include: intro.org

#+include: coloquio/Roger_Dingledine_privacidad_era_gran_vigilancia.org
#+include: coloquio/Gina_Gallegos_Garcia_La_ciencia_detras_del_anonimato.org
#+include: textos/Juan_Carlos_Perez_Perez_Riesgos_inherentes_en_la_privacidad_de_las_personas.org
#+include: coloquio/Daniel_Kahn_Gillmor_Autocrypt.org
#+include: textos/Antonela_Debiasi_Diseno_de_experiencia.org
#+include: textos/Tails_Sajolida.org
#+include: textos/Alejandro_Miranda_MPPA_panoptico_digital.org
#+include: coloquio/Luis_Fernando_Garcia.org
#+include: textos/Alfredo_Reyes_Hablemos_derecho_de_tor.org
#+include: coloquio/Lex_informática_Hablemos_derecho_de_Tor.org
#+include: coloquio/Jesús_Robles_Maloof_Enjambre_Digital.org
#+include: coloquio/Ignacio-Israel_Derechos_digitales.org
#+include: textos/Jacobo_Najera_La_internet_anonima.org
#+include: coloquio/Gus_Desnortear_tor.org
#+include: textos/Norman_Garcia_Monitoreo_censura_cibervigilancia_acoso_centroamerica.org
#+include: coloquio/Juliana_Guerra_Cuerpo_diferencia_y_derechos.org
#+include: textos/Raúl_Ornelas_Aventuras_y_avatares.org
