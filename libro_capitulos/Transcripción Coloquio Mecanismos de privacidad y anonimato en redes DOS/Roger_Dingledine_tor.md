# Tor

__Gunnar:__ Bueno pues para tener un poco más de tiempo para las preguntas
y demás vamos a arrancar un poco antes de lo que dicta el programa y vamos
a intentar llevar todo el programa según está estipulado. Estamos unos 15
minutos adelantados, lo cual me parece bien. Me da mucho gusto presentarles
a __Roger Dingledine__ que es el fundador del proyecto Tor y pues el sabrá
decirles.

__Roger:__ Ok, I'm sorry but don't speak English. I will be try to speak slowly
and i heard there are this app to traduce from your phone to listen to the nice
people from the back on FM radio. So, if you need help making that work ask
Gunnar. He'll go back and be one of the two nice people translating  me. If you
bang on the window slow down and speak more slowly. Also, if I'm talking to
quickly please stop me and I'll slow down and try to be clearer.

I'm Roger, i work on the project Tor and I'm going to tell you a bunch of
different things about Tor and we can talk about more after words. I'm going
try to give a lot of different things to think about. Tor is a non-profit  in
the USA, it's a foundation charity and it's also a program that you can use to
have more safety on the internet, it's a network of volunteers around the world
who run relays help wrap Tor traffic. It's also a bunch of people around the
world. Tor is a community of researches, developers and users?16.16 teaching
everybody why privacy matters. The found part on my perspective every city
where I go to has a research group at the university doing research on Tor.
There are people at this university thinking about how improve Tor performance
or how to improve Tor security of how to analyse this.

Tor is a program that you can install on your computer, usually Tor Browser is
the way to install it and the idea is you can browse the web without the people
watching you, learning what web sites where going to and without people on the
other side learning where in the world you are. Without any single point in the
middle being to track what we are doing. So, that's the basic idea for what Tor
tries to prove.

We have some number of users, it's an anonymized system so it's a bit
hard to know exactly how users we have but we have something like
2,000,000 people using Tor everyday. There's a research paper that
came out recently saying that there are more like eight to ten million
daily users at Tor. So it's a huge and growing community of people all
around the world how care about privacy. We are in an engineering
school so lets start by thinking about the threat model. What we can
try to protect? What we are worry about?

So we have some user called Alice and she's trying to reach some website called
Bob. Where can be the challenge be? What we are worry about? So, maybe the
attacker is watching Alice's local network, maybe is something using the
wireless in the building right now or maybe is in the local telephone company,
that's one option, and another option is maybe the attacker is on the website
side, maybe there are watching Wikileaks and he want to know
how is connecting to Wikileaks or maybe there are in the destination, maybe
is cnn.com and they want to know how is connecting to the website so they can
analyze to compare. 

Am I speaking well enough?, to slowly, clear enough? I guess that people for
how the answer no wont to be know in your heads cause don't understand what
I say. Perfect

Or maybe the attacker is somewhere in the middle, maybe is AT&T or Horizon,
or the Mexican telephone backbone and there are trying to watch the whole thing and
learn who is talking to whom. So, there are different places that
the attacker can be and we need to build the system that can keep you safe as
possible when people are trying to attack where your traffic is.

And other key point. Anonymity is not encryption. A lot of people
attach a lot companies and they say "we don't need Tor because we use VPN we
use encryption so we are safe" and the problem there is somebody watching your
traffic as it goes back and forth, encryption is good, you should use
encryption but they still learn who you are talking to, when you are talking to
them , how much you are saying, and all of the intelligence agencies, they don't
try to break encryption. Nobody tries to breaks encryption anymore, it's all
about: let's build a social graph with who is talking with whom, and then find
the person in the middle and then will break into their house and change
their laptop or something. So, the social graph is where the interesting thing
were all of the intelligence agencies they don't try to break encryption they
are look at this called meta data. This is a really creepy picture of the
american NSA guy. How many people here know the word metadata or heard
it before? Ok, great, rise your hands.

You can think that encryption as protecting the payload the traffic that your are
send and the metadata security as protecting how you are talking to, where you
traffic goes and things like that. So, this is important because interesting
documents that Ed Snowden brought out we learned more things without how intelligence
agencies work, not just in the US, but in many other countries. 

I actually only use the word anonymity when I'm talking too other professors
and researches or scientists around Tor. When I'm talking to my parents I told
them that I'm work on a privacy system because privacy is an important value.
Anonymity is scary and "I don't know if anonymity is good or bad" but privacy
is good. So, that hold to make them to understand what I'm doing. When I'm
talking with companies I told them about network communications security
because anonymity is scary and I think that Oracle's guy say that privacy
is dead, so companies don't care about privacy but companies care
about security, safety so maybe they want to hide which companies they are
investigating on the internet. I heard recently that Goldman Sachs,
huge investment company, uses Tor thoughout their company because they don't want
their competitors to learn which companies they're investigating to
decide whether to invest in. So thats for the company side. When I talk to goverments
and militaries I work on traffic-analysis resistance communication network.
This a very complicated phrase but again is the same security properties, it's
the same network. The goal is to learn how present things in a way
that each different group decides that they care about using the network.
And then there's a fourth category which is people how are blocked and censor
from connecting to websites they want to reach. There are people around the
world that can't reach BBC or they can't reach Facebook and we want to get all
the tools and allow them to do that.

So, How this Tor actually work? How are you build an anonymity or privacy or
security or safety system? The easy answer is you have some big central
service. This is a VPN work, this is how anonimizer work. So, you have some
central computer and all of the users ask a webpage and it goes to engage that
webpage and sent back. It's a very simple

[ Lost video ☹ ]

And, there is encryption. I'm not to talk about encryption today except for one
interesting point. The way we do crypto gets the property called "perfect forward
secrecy" and that means that if a few watch traffic at the Internet and you
reported and then you show up the next day and say; this traffic comes to my
relay I've to force to decrypt the traffic, you can't, because you don't have
anything anymore the lets it be decrypted. So that means in order to
attack the Tor crypto you need to attack while the person is actually sending
that traffic. You can't show up next day and try attacked them. 

That was the network level anonymity or the traffic level privacy. There is
a second piece about torens and that's the application level privacy. So, first
part was you can't learn which web sites is going to, the web sites can't learn
where I'm coming from. The second part is all the stuff in your browser. In
other words about cookies, there is a lot more besides that. It
turns out that every time you go to the web site your browser is happy to tell the
web site how many pixels width by how many pixes height your browser windows
is, is happy to tell the languages you speak, is happy to tell how many plugins
you have installed and literaly hundreds of things like that. This things can
identify you the web site. It doesn't tell necesary the web site what you name
is or where you are but it gives the ability to recognize you over time. If
I go to up log today it can learn enough about me that I'm going there
tomorrow. It can tell that it's the same ``me'', and that's dangerous because we
really want to give you privacy by default. You can then choose how much you
want to tell web site you go to. It make sense, to go Facebook over Tor and to
login to Facebook, if you want to do that, and maybe you tell them about where
you are but somebody watching the network connection still doesn't know where
you are going. Somebody watching Facebook still doesn't know where you are and
Facebook doesn't know where you are. Why Facebook need to know I'm in Mexico
right now? Even if I login to say "Hi! I'm Roger" Why should I tell them the
metadata where my traffic is coming from? They don't even know that. There are
a lot of different pieces of what anonymity can be. Is not just I want to hide
me from the destination.

## Tails

There are another ways of using Tor. I think we are going to have a talk later
on today about Tails. So, it's basically a live CD or a USB key it runs Debian
Linux and it has the entirely the operating system, all setup configured it way
should be. So, you have the applications do you should to have, and
they are configured safely, and you don't have any scary applications like microsoft's
word that screw up your privacy. The goal of Tails is have everything self-contained.
Many of the journalists who are working with classified documents from Ed Snowden
and so on, they use Tails in order to be safer when there sharing documents.

## Orbot

We also have released Tor browser for Android in addition to Windows,
Linux, Mac. We now offer for Android which is great, it's the same Tor browser.
The problem we have before on Android  was the Tor part works. The Network
level anonymity work but the browser level the application level privacy, the
cookies, and so on that was not the same Tor browser. So now is the same
program on all the different platforms and we give you the same level security.
We can't do iOS yet because Apple controls web browsers can run on iPhones and
the only allowed to run the apple browser which is really hard for us to make
that safe.

## Performance

A little bit of discusion about performance. Here is the graph of the
total traffic carried by the Tor network over the past eight
years. The purple line is the actual traffic of the network and the
green line is the amount of traffic can handle and you can see here
where the two lines meet. Tor was not fun to use.  Everything was slow
and you have to wait for someone else page to load for your web page
could load.  But now that the lines are more separated tor is more fun
to use because there's enought capacity in the network and your
traffic can go through more quickly. We will always need more relays,
thats something we should talk about later. You activate your
university and the university is a perfect place to run relays and
help to grow up the Tor network.

## Tor's safety comes from diversity

How we actually think about the safety that Tor provides? There are two ways to
think about how the network Tor is safety. One of the ways is  where are the
relays and actually we have at Tor network where all the relays are running at
MIT, Harvard, Princeton and at a small geographical area. In that case it would be
pretty easy to watch where are all the network connections, and you could see
all the traffic going through the Tor network and all the traffic coming out
and now it's possible to attack. So, the more relays we get and the
more different places they are then harder is for attackers to go to all of
this places a watch all this internet connections in order to attack Tor. So
more relays in more interesting places	is one way to ensure more safety. 

Another way to measure safety is what are the users doing?, why are they using
Tor? Around is a great example here. We have something like 50,000 people
around using Tor everyday activating for all the political dissidences, imagine
everyone which using Tor because they want to change their goverment and they
all try to be interesting from the perspective of the goverment on the run. In
that case the fact they are using Tor	is the self-interesting and dangerous.
Fortunely  most of the people in around are using Tor because around block
facebook, around block web comments they can't to get pictures of cats, kittens
and puppies, rainbows or wherever normal things the do on the normal Internet.
And because that censorships happens ordinary people install Tor in order to
do ordinary things. That ordinary things are critical for safety we need those
50,000. Ordinary people using facebook in order from the fact they are using Tor
to no self's be dangerous or interesting to the others. Make sense so far?

## Transparency for Tor is key

Another founding by Tor is transparency. So, it's an Open source / Free
software project. We give you the source code, but there's a lot more
than that. We also give you the design documents, what we are trying to build,
and we give you the specifications, Internet standars design of what
we do. So we tell you what we wanted, we tell you how where going to do it,
and then we give you the source code. If you are good reading source code and
compare that with the specification and say I introduced a bug. You say you
build this but you didn't or if you're a researcher and looking at design
documents and specs and you say when you want to build this, but said
build doesn't get that. Part of our goal is open source is a good start
but we need to give you everything see you can help us to state what are we did
it right. We also go to different places and do talks of Tor and meet people
and we public about it. My name is Roger the many developers are here and the
will tell their names also and this transparency from the project is really
importante for trust. I talk to a lot of people to say: "Ha Ha it's a privacy
project talking about transparency, ha ha that's a contradiction". It's not
a contradiction. Privacy is about choise, privacy is about control. We choose
to identify our selfs in order to make  a safer, stronger project and thats
a choice that's important for us to make because this open meet us, talk to us
and decide for your selfs whether Tor is making you safe.

## But what about bad people?

There's always some body in the audience who says "what about criminals? Are
you help that people". There are many answers for this. I'll try a few for now
and then we can talk more afterwards to try more answers. One of them is Tor
has millions of people using it every day and a few are bad people, thats the
same at the Internet, but a lot of people how are concern about bad people on
Tor, they think the Tor has ten users and they think that all are bad so what's
the point? and the answer is there are millions of people they are not seeing
because they are ordinary people going to facebook, google doing wherever
people do at Internet and you never see those people. That's one answer.
Another answer is you can thing it as the benefic to the people for be safe
and yes the bad people are benefic also so you have to balance. There is good,
there is bad, it's a tool. Like all tools there are two sides to it. But, thats
not point yet because in the Tor situation the good guys have very few options
for how be safe and the bad guys have a lot more options. If I try to be bad at
the Internet there are more ways I can do it. I can break into computers and
use them for my bot net where is the good guys have a few options.

So one way at looking that imagine what about terrorists question. Imagine
option one: I want to build a tool that works for a million people and works
for the next year and I tell you all about it so you can help me building it.
That's the Tor problem. Scenario two, I want to build a tool for twenty people
for the next week and I'm not going tell you about. That's the bad guy problem.
There are so many ways to solve scenario two. I get on a flame war in
Wikipedia, or or I hire something on the images ebay. If I don't have to tell
you about of my design and it doesn't have to scale and it doesn't have to last
there are so many more ways on doing that. Tor needs to build a system that scales,
it's transparent, and at last for decades. That's the good guys' problem and
that's what the good guys need.

## Tor website

Here is the Tor website run several countries around the world. We got "this
site is blocked for the content that it's contrary to the laws of the
State" down here. "Surf safely! This web site is not accessible". This is
whats you get when you go to torproject.org from Dubai or Saudi Arabia and so
on. Here's more examples "access to this site is permanently blocked", "The
website you trying access has been blocked". For this one (points out the down
part of the blackboard) there is and open sensor tell us your name and your
address and why do you think they censor this?  And get's more excited
on that. They tried make it fun. There like "Ooops!!!" we censor the internet,
it's fun, it's a mistake. We are not dictators. This is the first experience of
Tor for alot of people around the world. They see this pictures instead our
website.

## Tor metrics

How many people here know about the whole Egypt revolution at Tahrir
square, people trying changes the goverment seven years ago? (some people rise
up their hands). Awesome!!! I asked the same question in Taiwan years ago and
I got two hands I got two germans. Everybody else say I don't know what you are
talking about. Here's a graph, we started work on Tor metrics trying to
understand who uses Tor and how the network works. Heres a graph with the
trend of people using Tor in Egypt when that happend. You can see on the graph
where the block facebook because a spike of people using Tor and you can see
on the graph where the unplug Internet because everything disappears for
a while and my favorite part of this graph is there a lot of people after works
still using Tor because many people say "the militaries is on charge, the
surveil infrastructure is an error, we know still watching everything so i'm
going continue protecting my traffic"

And then so more recent graph here's a several people from Rusia in 2015. There
is 200,000 and so and is hard enough if the numbers are right it could be
easily five times this number. It could be 1 million or so and then there's
a huge spike and then it goes down and then up and down and again. For
a while we try to figure out what causes hundred of thousands of people to
suddenly start to using Tor in Rusia and then I talk with somebody at facebook
who says they have exactly the opposite this graph internaly. In facebook, they
have a bunch of people using Facebook in Russia and then facebook is blocked
and all disappear and then it works again and then facebook is blocked. So they
have the inverse of this graph where hundred of thousands of people where
switching to using Tor when Russia block facebook and then when the blocking go
away those people decide "I can just connect directly" and there are more
interesting graphs on metrics.torproject.org you can go look.

## Censorship

How does censorship work? We talk before about public relays, you
go through three relays in order to anonimize your traffic. The first pieces of
censorship is they block the website so it's hard download Tor, but you get it
from a friend or you get it from a mirror or you get it from somewhere and at
that point the next step for censorship is they block public relays. There
a big list there about seven thousand. They get this list and they blocked by
the IP address and then makes hard to people to connected on the Tor network
because you can't get there. Everything is blocked. The defense against that is
who we call bridge relays and the idea is you rely onto volunteers to run
bridges and the difference is they are just relays but they are not in a public
list they are not easy for the attacker to get. In this case we change the
problem from How we publish seven thousand IP address publictly
without China raising an alarm? , It's a very hard problem, to How we take thousands of
volunteers addressand give one a time to the people who need them without
lading China alarm all of them? So, that's the goal with bridge relays.

That work for several years until the next piece on the map. The next
step was, Iran buyed a fancy computer from Nokia, was called Deep
Packet Inpection or DPI. The idea for DPI is, it looks at the protocol
going back and for and it says "thats SCARP, that's bitorrent, that's
HTTP, that's Tor" and at that point just cut the Tor traffic even if its going
to an IP address that they doesn't recognize. Iran did this on early 2011
they have a particular rule that they look that, for the geeks in the audience,
they work looking at the Diffie-Helmann parameter the prime we use for
the RTLS handshake and we use a better prime than in the pass we uses but we
were different because they can distinguish normal web browser to normal web
servers from Tor they could cut that. Fortunely we have a great engineer with
the other non-profits how looked this and start messing around and in one day
they found how they are blocking it and when fix it all the traffic went back
up. That was a fun early example of DPI attack against Tor.

## Pluggable transports

A long Tor answer is were we call pluggable transports. The idea is the Tor
network takes care your privacy, anonymity and a few things and then you
pluggin a traffic transformer or a transform before it. The Tor traffic gets
transform in somehow in the something to the adversing on the censor is not
enable to block. you turned into Skype or you turned into HTTP or you turned
into something else. There are a lot of research projects out there building
different pluggable transports to make the traffic look like something that
they don't want to block. So far so good?

## Blue Coat

How many people here know the story about blue coat and Syria? (~4 four people
on the audience rise up their hands) You rise more hands than the Toronto
Defcon I got one hand last week on Toronto. This is the history every should
need to know.

This was three to five years ago, a bunch of people from the hacker group
Anonymous were looking a round of computers on Syria and they found
a misconfigured http server in Syria and it was offering Gigabytes of plaintext
unencrypted log entries from a computer called blue coat. A company and
a product called blue coat. Blue coat is one of this surveillance/censorship
systems. It's a company in Sunny Valley, California. It does DPI and
surveillance to basically censor Internet for you and the log file that they
found on this computer on Syria were billions of lines saying "This IP
address tried to go to this website and I stop it, this IP address trying to
go to this website and I allowed".

The entire web browsing traffic from Syria
was availiable from this misconfigured server running by a company in America
called Blue Coat. My country has a law against running surveillance and
censorship infrastructure in several countries, Syria is one of them, this
people go to Blue Coat and said "What's going on? You are breaking the law, you
are running a surveillance infrastructure in Syria" and Blue Coat said "No,
we're not" and then anonimous said "But the top of the file says Blue coat
version 1.5.2 What you mean with you're not?" and then blue Coat said "Oh, we
sold those to Dubai. How we were suposse to know they open it on Syria? We
follow in the law, we can't control what happends to our computers".

Anonymous said "But you still given them updates everyday" and then blue coat
said "We turn that off, we disable it, we stop that" and then anonymous logged into
the update server and view the serial numbers from the blue coat servers in
Syria and they go their updates. Blue Coat lies every step of the way and the
end of this sad story is Blue Coat got a Human Rights Award from the
US State Department for their cooperation in investigating this. They are
a terrible company, the problem is there are so many companies like that. They
are happy to build the surveillance system. Cisco build this censorship/
surveillance system in China and they also build it in Burma and then they got
outcompeted by another company called Fortinet and there is a terrible
picture on the Internet of this sales guy of Fortinet shaking hands
with the military leader in Burma over the new deal they had made.

In Tunisia, I was at a conference some years ago, right after the
Tunisia government change, and the head of the Internet agency was
doing a speech, saying, yes, we use filtering.
We pay this American company one million dollars every year to use
this filter. Think how much food they can buy with that maybe shouldn't
censor our
internet and he was also explained they get it cheap, one million dollars is
the disount price because then this smart filter goes to Saudi Arabia and
saids "it works in Tunisia you pay full price". This huge busines world out
there is terrible. The other exciting part of the Tunisia story they don't
actually run they smart filter they outsource that to some company in France,
He won't tell me which one, so there some other goverment they gets to see
everything happens on the Tunisia internet and to guest to desired web pages
you get. That includes censor and surveillance on the Tunisia military from
some French company. This is not just civil liberties thing or human rights
thing, this is a national soverignty thing. This is aren't you in control of
your country anymore if you letting some foreign company decide what you
internet actually is.

A few years ago I go to the German foreign office where they had a meeting to
decide Should europe try to stop European companies from  building this things and
selling on the surveillance countries. There is a company in Italy called Hacking
Team that goverments around the world go to, basically, buy attacks. They pay
to this italian company and the italian company brings on the anybody, install
backdoors anywhere. The German foreign office was trying to decide how we can
make laws to stop this, how can make laws to make it hard for europe companies
to do this? It was an exciting meeting because i was the only engineer on that.
There are all laywers, diplomats and policy people. Every time they had
a technical question they tell me "Roger what's the technical community think
about this?" and I was the one person representing every engineer in the world
at the German govermens at meeting to decide what to do. The other fun part of
that there was a telephone company engineer from Dubai who was hanging around
outside. How many people here know the term Lawful intercept? all of the
internet routers there are build are required by the USA and Europe for them
have a backdoor. It's an extra port that you can plug a cable into and it lets
you spy the traffic thats going through that internet router. They call it
lawful intercept. I call it backdoor. They explaind it's public, it's documented
so it's not secret, it's not a backdoor. Maybe we called a front door but the
problem is there is a way to spy on everything happens and maybe in America and
Mexico and so on are judges or a judicial process and you go to a court and
somebody decides whether is legal to spy on the internet router or not. But
when you're in Saudi Arabia and you plug in on an internet router and somebody
saids "What's that port for?" and the answer is "Oh, that's the Lawful intercept
port" and the prince saids "I'm the law, plug it in!" Maybe in America are checks
and balances but the same design tools to other parts of the world where are
not the same checks and balances and actually they are not work very well in
America neither, but separate discussion. He was explaining, "Look, you guys
forced us in having backdoors on this internet routers and now you're
complaining when we use them to looking people's traffic. It's your fault that
the system is so easy to spy on. You required by your laws that it's easy to
spy on and now you're unhappy when we use the back door that you build"

I think that's a critical thing that we need to think more about where the USA
and Europe make decisions to the rest of the world and they're not good
decisions for the USA and Europe and then also not good decisions for the rest
of the world.

Australia censors their internet, England censors their internet, Denmark
censors their internet, Sweden censors their internet and that means that when
countries go to China and say you're not doing it right, censoring is bad you
shouldn't censor, China saids "We just keep our citizens safe just like
everybody else, everybody else censors, we censor. No problem, what you are
picking on us?" The fact more and more western countries are doing it, makes
harder described as a bad thing. It's becoming more and more normal to censor
the internet and are more and more countries doing it and this a terrible track.

This are the official internet police in China. I don't know if you see this
cute pictures before but their try to make it fun, censor the internet is fun,
remember no to surfing in site that are unsafe, remember no to go to website
you shouldn't to go to. It's fun, it's easy. China puts a lot of money into
surveillance and censorship and they are much far on the race than every
another country. Tor works very well everywhere except China and it's hard to
use Tor successfully in China.

Wikipedia is another fun example, you can read Wikipedia over Tor but you can't
edit Wikipedia over Tor	and that's because Wikipedia has a problem with jerks
on the internet who connect to Wikipedia and they wants to write "Wikipedia
Suck" on as many pages as they can. Because they are trying to make Wikipedia be a worse
site. They ended blocking the IP address of all of the jerks and then the jerks
use proxies and block all the proxies and then the jerks break on the windows
computers so they block all the Windows computers and Wikipedia has a black
list of millions of IP addresses that are not allow to edit Wikipedia. They
claim Wikipedia is built by everybody, anybody can edit it but they have
a growing list with people who can't edit it. If you look at the graphs of
contributions to Wikipedia it looks like this (rise his hand like a peak). Ten
years ago Wikipedia was a great web site, everybody was enabled to edit and now
the contributions are going down. There is a group doing cool social science
research on whats missing Wikipedia? How you're measure the loss to Wikipedia
from losing all of this users? All of this people how would to edit all of
this pages but you stop them. Imagine a page about abortion or the Republican
party of the USA, or some sensitive topic that allow to people don't want to
edit because they don't want to identify them selfs what they doing. How we're
measure what's missing? How much damage just happend to Wikipedia based on the
fact that they're blocked or preventing people from editing it?


## Edward Snowden

Another topic, this is the first slide of a several slides the Ed Snowden broad
out about Tor. Everybody here knows about Edward Snowden? I hope. The
first fun part is that Tor sticker upon the picture. That was the first time
a lot of journalists learned about Tor. So there were many journalist
contacting us saying "So Ed Snowden wrote this thing called tor can tell me
about it" and actually we were like "No, we are a non-profit, we've been here
the whole time, happy to tell you about Tor". the other fun part of this, Ed
brought up as many documents as he could find, he went everywhere he could to
find everything he could find about Tor in order to give to us so that we would
know as much as we can about the NSA and GCHQ and other groups attacking Tor. 

So this particular slide is from June the 2012 and it was at a conference that
happened for one week in the US and one week in England, and te conference was
named Remission 2, I don't know what a remission is, but the 2 makes it sound
kind like there was a Remission 1, I don't know, I talk to somebody recently
who said that they recently held another one, a Remission 3 last summer or
something like that, so they're definitely trying to thinking through how to
attack systems like Tor and there's one fun quote about the "Tor stinks" slides
basically the Tor stinks slides were "we tried to attack Tor like this and it
didn't work, we tried to attack it like this and it didn't work, we don't know
how to attack it" and there is this quote "Tor is still the king of high
secure, low latency Internet Anonimity, there are no contenders for the throne"
I didn't say it they said so, we've been thinking for a while doing t-shirts or
doing a version of our website with endorsement from the NSA rather than what
our press people say.

Ok, so everything I been talking about so far is the first use of tor, I want
to go to a website and I want to be safe while I'm doing it. there is another
way of using tor called onion services or hidden services and the idea for that
is I want to be able to run a website or some other service in a way that
nobody knows where it is but you can so reach it over the tor network, so
basically you take that idea of the 3 relay circuit and you glue two of them
together so one side is using tor to keep themself safe and the other side is
using tor to keep themself safe and they meet in the middle and that means the
user doesnt have to know where the destination is, the destination doesn't have
to know where the user is but they can still talk to each other over tor.

and the way this works on practice for websites is called onion addresses, it
maybe hard to see [in the slides] is a bunch of random letters dot onion is the
address the tor network provides nad the cool thing about the naming process is
that that bunch of random letter is the hash of the public key of the onion
service so this gives you some really cool security properties one of the big
ones is theyre self authenticating if you know the onion address then your
browser makes sure that youre talking to the right destination it makes sure
youre talking to somebody who knows the private key that matches the name that
you typed in. So in the normal internet you type https facebook dot com and
then you go somewhere and you get a ssl certificate promising that this
destination is facebook but the way that promise works is based on something
called Certificate Autority and your browser has a list of 300 companies that
it's willing to believe when any of those companies say "this is facebook.com"

So Turkish telecom runs a certificate autority, China telecom runs
a certificate autority there was one called Diginotar in the Netherlands and
they ran a certificate autority until somebody broke in and started making fake
facebook and google and Tor project ssl certificates, so the big problem there
is there are 350 of this companies and I can brake into anyone of them and then
pretend to be any website on the internet and Tor's onion services gets around
that by not needing to ask a third party, is not about "I trust this company"
or "they promise that they checked", the name itself lets you check that you're
going to the right place and that means there's no need for some external
party. Also because it's all inside the Tor network you get end to end
encryption, you know who you're talking to, and you get a complete encrytion
all over. And the person running the onion service doesn't have to be reachable
on the internet, so you can run a website and people can go to it even if you
don't have a public ipv4 address, so even if you're not in the internet the way
the normal websites are, you can still offer services, you can still run
a blog, things like that.

Another cool thing is people run their ssh server at home or at work behind
a onion service and then they firewall their internet connection so that
there's no way to connect in except going through this Tor  channel that
provides end to end security, authentication, encryption and that limits their
surface area so that is much harder to attack their computer, they block
everything coming in except this one Tor connection that they trust.

Ok, so a few years ago I was talking to some law-enforcement group and they
said "we just busted *inaudible* there were some websites on the internet
selling drugs, we took it down and the Tor network... their traffic dropped in
half, we just took down half of the Tor network by busting this drug website"
and I was thinking "gosh, that a lot of traffic. I wonder if thats actually
true" so then we started looking at how much traffic on the Tor network has to
do with onion services at all, and here is the graph over time, remember on the
last graph of total tor traffic on the tor network, that was about 125 Gbps and
this is around 1 Gbps, so the question is what fraction of the overall tor
network [traffic], has to do with onion services at all, there is some cool
math to be done but the answer is about 3% so 97% of tor traffic is people
going to websites like facebook and amazon and gmail and 3% is people going to
onion services, so its a tiny fraction of what happens on Tor, so if you hear
that somebody just busted some onion service and the Tor network changed a lot,
it can't have changed a lot, only 3% of the Tor network has to do with onion
services

And this is different from the picture of the iceberg that everybody always
sees with the dark web, TIME magazine a while ago talked about how there are 99
other internets out there and you can only get to them through the tor browser,
so the internet that you know about is just the surface and it goes deeper and
deeper, is just non-sense, it's just all garbage they're trying to sell ads,
they're trying to sell newspapers, there are something like 7000 websites
available as onion services right now total, that compares to the millions or
billions of websites on the bigger internet, so onion services are a tiny toy
that I wrote 14 years ago and it has some cool potencial, but basically nobody
is using them yet, and a while ago BBC did an article saying "you can buy drugs
on the internet and here is how" and their comment section was full of people
saying "wow, thanks, this is great I dont have to go get *inaudible* on the
street corner, thanks for telling how to buy drugs more safely" and it was full
of people commenting, and then they did an article a week later and we bought
some and we have them tested and they were really good". So think about what
BBC's motivation is, are trying to give you news? are they trying to make you
click on the advertisements on their website? are they trying to stay relevant
compared to CNN or FOX or something? So journalist are trying to give you
a picture of icebergs everyday to scare you and make you confuse, the darkweb
is tiny and there isn't much going on.

That's it. What would you think is the biggest website on the darl web right
now?
- Facebook Facebook, yes. Know you would think "OMG, but what about this, what
	about that" This is a blog post made by Facebook it may be hard to tell but
	it's up here at amm... Facebook over Tor, this was made on April of 2016,
	were Facebook said "we've  been tracking how many people are connecting to
	our website over Tor and the 30 day shift in average was as up 2 years ago,
	a million people login into Facebook over Tor", and I don't know how to count
	to a million, that's a lot of people, but imagine Facebook has a billion
	accounts, so this is 1 in a thousand Facebook users were using Tor two Aprils
	ago, that's 0.1% of facebook was using Tor, so that's amazing, that's a huge
	fraction, and Facebook said the same thing, Facebook said "our users are
	clearly wanting privacy, we want to set things up for them so that they can
	be as safe as they wanted to be", because Facebook has users in Turkey, and
	if you go to Facebook.com from Turkey and the Turkish government intercepts
	that and gives you a fake Facebook you're screwed, there's no way for you to
	tell that you're going to the wrong website, so they want to give their users
	a way to know that they're reaching Facebook. It is not dark, think of it
	like HTTPS except with metadata security. One of the callenges I see is there
	are all of these companies called Threat Intelligence companies, that try to
	tell you if your information shows up on the dark web, and one of them told
	me a while ago "I found a copy of facebook on the dark web" and I was
	thinking "no, it's a website it's called Facebook, you can get to it over
	HTTP, you can get over it over HTTPS, you can get to it over its onion
	address, it's the same website", there's not a separed thing called the dark
	web, it just another set of security properties for a way of communicating on
	the internet, it's like HTTPS, but better.

So there's another cool use cases for onion services, which are just started to
get interesting, and one of them is what's called SecureDrop, is a whistle
blowing platform for connecting, journalist to people who have documents or
stories or information about corruption or people doing illegal or bad things,
and there are 30 or I think this number is up to 50 or something different
major organizations, New York Times, Washington Post, the biggest newspaper in
Canada, I actually don't know if there's a secure drop in any mexican
organization, i'll be curious to know if there is one. So there are a bunch of
different organizations out there where you can reach their journalist in
a safe way, because the alternative is send us emails, and we promise not to
tell people who you are, and that's bad because they know who you are and they
could give you *inaudible*, so you have to trust them, they promise you would
be safe, it's also bad because anybody watching the New York Times' internet
connection get to see who sends the mail, so you need some way to safely
interact with them in a way that they can ask you questions but you stay in
control of your own privacy.

Another different example, all of those before were websites, where sombody
wants to set up a website where nobody knows where it is. Ricochet is an
instant messaging tool that uses onion services. How many people use XMPP or
Jabber or iMessage or Signal, so all of those instant messaging designs have
the same architecture where there is a computer in the middle that knows where
you are and who you are talking to and knows all of your friends, so if i'm
talking over Jabber or Signal or other things, they know when i'm talking, they
know who i'm talking to and that means that if i'm trying to attack Signal or
XMPP there is a central place I can go and break in and learn who is talking to
who and how much they're saying. There's end to end encryption if you're lucky
and that's good, you don't know what people are saying, but again is who
they're talking to that's important, so the goal of ricochet

[ Lost video ☹ ]

Onion share is another fun example, so imagine that you're a journalist and
you're holding the Ed Snowden documents and you want to give them to the
journalist that's sitting next to you at the desk, what do you do? do you email
them? you put them on dropbox, that will be safe, except Dropbox shares all the
files that they get, do you set an FTP server and then you use FTP and so on,
that'd be great if you understood *inaudible*. So there aren't many ways of
safely sending large files around. OnionShare is an easy to use program on
Windows, Mac, Linux and you give it a file and spins up a web server and it
gives it an onion address and then you get this URL and you send it to your
friend over Signal or Ricochet or whatever, and they paste it in their Tor
browser, and the file transfers and then your webserver disapears, your onion
address disappears and there's nothing left, so it's an easy way to move files
around the internet in a way that there's no middle, there's nothing to attack,
there's no place to look at when they're trying to learn which journalists are
talking to which journalists.

Ok, so Tor is not perfect, there a number of categories of problems to think
about. The biggest problem is OpSec or operational security problems, imagine
you go to a blog over Tor and you write your blog post and put your name on the
bottom, maybe that was good because you wanted it to, or maybe it was
a mistake, you forgot that you were trying to be safe, and trying to be
anonymous, so there are so many ways of screwing up, "i always use Tor when
i go to this website, but today I forgot, and know they have my IP address",
every time I talk to an FBI person they tell me a story about how they were
trying to find somebody who was using Tor and it was really frustrating because
they couldn't find that person and they kept, they were patient they kept
watching and eventually the person screwed up and didn't use Tor and then they
busted them. So using Tor the wrong way is by far the hardest thing for us to
try to fix.

Another issue is browser metadata, every time firefox puts out a new version
there are new that you can be tracked using the new features. Like, there's
a new feature that Google just made a few years ago, where websites can learn
about how power there is in your battery, so I go to a website and the website
says "how is your battery", and I say "my battery is 67.74% full" and then I go
to another website and it asks me about my battery, I give them the same
answer, and there aren't many Tor users right now with exactly that amount of
batttery life, so that's something we need to protect and hide from the
websites, so that we are not identifiable, and every new version of every
browser has tracking things like this that need to discover and research and
fix.

And then a third issue is that web browsers are not as safe as they should be,
they're huge complicated programs, there's always some bug on rendering images
or doing javascript or going to certain webpages and bad people can take
advantage of this bugs to break into your computer when you go to the wrong
website, and that's true on Firefox, that's true on Chrome, that's also true on
Tor Browser.

And then the fourth thing to think about public traffic analysis, where large
intelligence agencies watching key pieces of the internet maybe watching enough
traffic that they can try to figure it out who's connecting where.

Ok so how can you help? So, you're in a university, with good bandwidth, you
should run relays, every relay has an exit policy, that lets you say which
addresses you want to let people reach, so you can run a non exit relay where
you're only inside the Tor network, you're never that last point that connects
to the website, and most people will never notice the network *inaudible*
you're doing, so you should do that.

Also, please help everybody else to understand how Tor works, when your friend
shows you a picture of an iceberg, teach them about onion services rather than
the dark web. There are open research problems, there's a conference that
happens every year called PetSymposium.org it will probably be in Canada in two
years and we also have a donation site.



[1.14.43]
